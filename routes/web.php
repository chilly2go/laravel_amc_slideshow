<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/f', 'OccasionsController@index');
Route::get('/f/{foldername}', 'OccasionsController@folder');
Route::get('/f/{foldername}/d/{directory}', 'OccasionsController@occasion');

Route::get('/o/{occasion}', 'SlideshowsController@index');
Route::get('/o/{occasion}/create', 'SlideshowsController@create');
Route::get('/o/{occasion}/{slideshow}', 'SlideshowsController@edit');
Route::get('/o/{occasion}/{slideshow}/time', 'SlideshowsController@time');
Route::post('/o/{occasion}/{slideshow}/storetime', 'SlideshowsController@storeTime')->name("slideshow.storetime");
Route::get('/o/{occasion}/{slideshow}/generate', 'SlideshowsController@generate');
Route::get('/o/{occasion}/{slideshow}/generate/reset', 'SlideshowsController@resetslide');
Route::get('/o/{occasion}/{slideshow}/reset', 'SlideshowsController@reset');
Route::get('/o/{occasion}/{slideshow}/delete', 'SlideshowsController@delete');
Route::post('/o/{occasion}', 'SlideshowsController@store')->name("slideshow.store");
Route::put('/o/{occasion}', 'SlideshowsController@update');

Route::get('get-video/{slideshow}', 'SlideshowsController@getVideo')->name("slideshow.getvideo");

