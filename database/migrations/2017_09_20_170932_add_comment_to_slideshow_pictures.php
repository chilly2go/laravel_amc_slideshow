<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddCommentToSlideshowPictures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::table('slideshow_pictures', function ($table)
        {
            $table->string('comment')->default('');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::table('slideshow_pictures', function ($table)
        {
            $table->dropColumn('comment');
        });
    }
}
