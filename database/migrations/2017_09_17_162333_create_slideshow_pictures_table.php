<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideshowPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        $tablename = 'slideshow_pictures';
        // table structure
        Schema::create($tablename, function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('slideshow_id')->unsigned();
            $table->integer('picture_id')->unsigned();
            $table->integer('order')->unsigned();
            $table->integer('duration')->unsigned();
            $table->timestamps();
        });
        // foreign keys
        Schema::table($tablename, function ($table)
        {
            $table->foreign('slideshow_id')->references('id')->on('slideshows')->onDelete('cascade');
            $table->foreign('picture_id')->references('id')->on('pictures')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('slideshow_pictures');
    }
}
