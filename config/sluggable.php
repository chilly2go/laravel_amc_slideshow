<?php
/**
 * Created by PhpStorm.
 * User: cookie
 * Date: 12.09.2017
 * Time: 19:03
 */
return [
    'source'          => null,
    'maxLength'       => 60,
    'method'          => null,
    'separator'       => '-',
    'unique'          => true,
    'uniqueSuffix'    => null,
    'includeTrashed'  => true,
    'reserved'        => null,
    'onUpdate'        => false,
];