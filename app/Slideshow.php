<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    protected $fillable = [ 'name' ];
    protected $casts    =
        [
            'generated' => 'boolean',
        ];
    
    public function sluggable ()
    {
        return [
            'slug' => [
                'source' => [ 'occasionSlug', 'name', ],
            ],
        ];
    }
    
    public function saveOrder ($request)
    {
        $pictureIds = array_flip(array_flatten(collect($request->pictures)->map(function ($picture)
        {
            return collect($picture)
                ->only([ 'id' ])
                ->all();
        })->toArray()));
        foreach ($this->fresh()->pictures()->get([ 'id', 'order', 'picture_id' ]) as $picture)
        {
            if ( !isset($pictureIds[$picture->picture_id]))
                dd('slideshow.php', $picture->picture_id, $pictureIds, $request->changedid, $request->pictures,
                   $picture);
            $picture->order = $pictureIds[$picture->picture_id];
            $picture->save();
        }
    }
    
    public function pictures ()
    {
        return $this->hasMany(SlideshowPicture::class);
    }
    
    public function getPath ()
    {
        return storage_path('app/slideshows');
    }
    
    public function getStoragePath ()
    {
        return 'slideshows';
    }
    
    public function getStorageFilename ()
    {
        return $this->getStoragepath . '/' . $this->getFilename();
    }
    
    public function getFilename ($occasion = NULL)
    {
        if (is_null($occasion))
            $occasion = $this->occasion()->first();
        
        return $occasion->slug . '/' . str_slug($this->name, '-');
    }
    
    public function occasion ()
    {
        return $this->belongsTo(Occasion::class);
    }
}
