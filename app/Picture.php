<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Glide\GlideImage;

/**
 * App\Picture
 *
 * @property int                 $id
 * @property int                 $occasion_id
 * @property string              $name
 * @property string              $thumbname
 * @property string|null         $ppath
 * @property string|null         $tpath
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Occasion  $occasion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereCreatedAt( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereName( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereOccasionId( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture wherePpath( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereThumbname( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereTpath( $value )
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereUpdatedAt( $value )
 * @mixin \Eloquent
 */
class Picture extends Model
{
    protected $fillable = [ 'name', 'thumbname' ];
    
    public function createThumb ($filepath)
    {
        $this->ppath = storage_path('app') . "/" . $filepath;
        $this->tpath = storage_path('app/public/thumbs') . "/" . $this->getfilename($filepath);
        $this->thumbCreation();
        $this->save();
    }
    
    private function getfilename ($path)
    {
        $tmp = array_slice(explode('/', $path), -3);
        
        return str_replace(" ", "-", implode("-", $tmp));
    }
    
    public function thumbCreation ()
    {
        GlideImage::create($this->ppath)->modify([ 'w' => 300, 'h' => 150, 'fit' => 'max' ])->save($this->tpath);
    }
    
    public function createSlideshowPicture ($to, $w = 1280, $h = 720)
    {
        GlideImage::create($this->ppath)
                  ->modify([ 'w' => $w, 'h' => $h, 'fit' => 'fill', 'bg' => '000000' ])
                  ->save($to);
    }
    
    /*    public function slideshow ()
        {
            return $this->belongsToMany(Slideshow::class);
        }*/
    
    public function occasion ()
    {
        return $this->belongsTo(Occasion::class);
    }
    
    public function slideshows ()
    {
        return $this->hasMany(SlideshowPicture::class);
    }
    /*    public function slideshows ()
        {
            return $this->belongsToMany(Slideshow::class,'slideshowpictures')->withPivot('order')->withTimestamps();
        }*/
    /*public function newPivot (Model $parent, array $attributes, $table, $exists, $using = NULL)
    {
        if ($parent instanceof Slideshow)
        {
            return new SlideshowPicture($parent, $attributes, $table, $exists);
        }
        
        return parent::newPivot($parent, $attributes, $table, $exists, $using);
    }*/
}
