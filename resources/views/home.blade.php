@extends('layouts.app')

@section('content')
    <div class="container" >
        <div class="row" >
            <div class="col-md-8 col-md-offset-2" >
            <!--<div class="jumbotron" >
                    <h2 >Logged in or not. Testing here.</h2 >
                    <p >
                        @if (Auth::User())
                u r logged in
@else
                u r unknown
@endif
                    </p >
                </div >-->
                <div class="panel panel-default" >
                    <div class="panel-heading" >Dashboard</div >


                    <div class="panel-body" >
                        <h3 class="media block" >Anlässe:
                            <a class="media-right" href="/f" title="Bilder für weitere Anlässe laden" ><i
                                        class="fa fa-plus-square "
                                        aria-hidden="true" ></i ></a >
                        </h3 >
                        <ul class="list-group" >
                            @forelse ($occasions as $occ)
                                <li class="list-group-item" ><a href="\o\{{ $occ['slug'] }}" >{{ $occ['name'] }}
                                        ({{$occ['root_folder']}}) <span
                                                class="badge" ><span title="Geladene Bilder" ><i
                                                        class="fa fa-lg fa-photo" ></i > {{$occ->pictures()->count()}}</span > | <span
                                                    title="Slideshows" ><i
                                                        class="fa fa-lg fa-th-large" ></i > {{$occ->slideshows()->count() }}</span > | <span
                                                    title="Generierte Slideshow-Videos" ><i
                                                        class="fa fa-video-camera fa-lg"
                                                        aria-hidden="true" > </i > {{$occ->slideshows()->where('generated',true)->count() }} </span ></span ></a >
                                </li >
                            @empty
                                <p >Keine Verzeichnisse gefunden.</p >
                            @endforelse
                        </ul >
                    </div >

                </div >
            </div >
        </div >
    </div >
@endsection
