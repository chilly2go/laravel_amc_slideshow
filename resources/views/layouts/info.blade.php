@if(\Illuminate\Support\Facades\Session::has('flash_message'))
    <div class="form-group" >
        <div class="flash-message alert alert-success {{        \Illuminate\Support\Facades\Session::has('flash_message_important') ? 'alert-important':''        }}" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button >
            {{ session('flash_message') }}
        </div >
    </div >
@endif
