@extends('layouts.app')

@section('content')
    <div class="" >
        <div class="row" >
            <div class="panel panel-default" >
                <div class="panel-heading" >Anlass auswählen, dessen Bilder geladen werden sollen</div >

                <div class="panel-body" >
                    <ul class="list-group" >
                        @forelse ($occasions as $occasion)
                            <li class="list-group-item" ><a
                                        href="\f\{{ $folder }}\d\{{$occasion}}" >{{ $occasion }}</a ></li >
                        @empty
                            <p >Keine Verzeichnisse gefunden</p >
                        @endforelse
                    </ul >
                </div >
            </div >
        </div >
    </div >

@endsection