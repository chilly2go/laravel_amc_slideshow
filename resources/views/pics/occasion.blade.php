@extends('layouts.app')

@section('content')
    <div class="container" >
        <div class="row" >
            <div class="panel panel-default" >
                <div class="panel-heading" >Anlass: {{ $occasion->name }} ({{ $occasion->root_folder}})</div >
                <div class="panel-body" >
                    <p >{{ $occasion->pictures()->count() }} Bilder wurden geladen.</p >
                </div >
            </div >
        </div >
    </div >
@endsection
