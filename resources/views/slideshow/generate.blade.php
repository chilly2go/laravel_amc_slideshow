@extends('layouts.app')

@section('content')
    <div class="container" >
        <div class="row" >
            <div class="col-md-8 col-md-offset-2" >
                <div class="panel panel-default" >
                    <div class="panel-heading flex flexbetween flexrow" >
                        <div class="cell" >Video-Slideshow "{{$slide->name}}" erstellen</div >
                        <div class="cell" >{!! html_entity_decode(link_to_action('SlideshowsController@index',
                                                        ' zurück',
                                                        ['occasion' => $occasion->slug],
                                                        [ 'title'=>"Zurück zur Übersicht"])) !!} |
                            {!! html_entity_decode(link_to_action('SlideshowsController@resetslide',
                                        ' Neu generieren',
                                        ['occasion' => $occasion->slug, 'slideshow' => $slide->id],
                                        [ 'title'=>"Zurück zur Übersicht"])) !!}</div >
                    </div >
                    <div class="panel-body" >
                        <ul class="list-group" >
                            <li class="list-group-item" >Video - Datei<p class="box-solid" ></p >
                                <video controls
                                       src="{{route('slideshow.getvideo',$slide->id)}}" preload="metadata">
                                    <source src="{{route('slideshow.getvideo',$slide->id)}}" type="audio/mpeg" />
                                    Dein Browser unterstützt das Video-Tag nicht.
                                </video >
                                {!! link_to_route('slideshow.getvideo',"Direkter Link",['slide'=> $slide->id]) !!}
                            </li >
                            <li class="list-group-item" >FFMPEG Ausgabe<p class="box-solid" ></p >
                                <pre >{{$ffmpeg_output}}</pre >
                            </li >
                        </ul >
                    </div >
                </div >
            </div >
        </div >
    </div >
@endsection
