@extends('layouts.app')

@section('content')
    <div class="bottom" >
        <h3 >Testing Vue Draggable - Sortable</h3 >
        {!! Form::model($slide, ['route' => ['slideshow.store',$occasion->slug]]) !!}
        {!! Form::submit('Speichern',['id'=>'triggerDragsort']) !!}{!! html_entity_decode(link_to_action('SlideshowsController@index',Form::button('Zurück'),['occasion' => $occasion->slug])) !!}
        <div class="form-group" >
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', NULL, ['class' => 'form-control']) !!}
            {!! Form::hidden('id',$slide->id) !!}
        </div >
        <div style="padding-bottom: 1em;" >
            <picture-dragsort ref="dragsort"
                              :picturesactive="{{ $picsUsed }}" :pictures="{{ $picsAvail }}"
                              :slug="{{json_encode($occasion->slug)}}" :slideid="{{$slide->id}}"
                              :basepath="{{json_encode(asset("storage/"))}}" ></picture-dragsort >
        </div >
        {!! Form::submit('Speichern') !!}
        {!! Form::close() !!}
    </div >
@endsection
