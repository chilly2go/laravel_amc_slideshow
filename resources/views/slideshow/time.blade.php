@extends('layouts.app')

@section('content')
    {{--<div class="container" >
        <div class="row" >--}}
            <div class="col-md-12 " >
                <div class="panel panel-default" >
                    <div class="panel-heading" >Zeiten und Kommentare für Slideshow {{$slide->name}} ({{$occasion->slug}}) | {{$slide->status}}</div >
                    <div class="panel-body" >
                        {!! Form::model($slide, ['route' => ['slideshow.storetime',$occasion->slug,$slide->id]]) !!}
                        <div class="flex flexwrap space-around bottom">
                        @forelse($slide->pictures()->orderBy('order')->get() as $pic)
                            <div class="picture flexgrow col-md-3" >
                                <img src="{{asset("storage/thumbs/").'/'.$pic->picture()->first()->thumbname }}" >
                                {!! Form::hidden('id[]', $pic->id) !!}
                                <div class="input-group" >
                                    {!! Form::label('comment', 'Kommentar', ['class' => 'input-group-addon']) !!}
                                    {!! Form::text('comment[]', $pic->comment, ['class' => 'form-control']) !!}
                                    <span class="glyphicon glyphicon-font input-group-addon" aria-hidden="true"></span>
                                </div >
                                <div class="input-group" >
                                    {!! Form::label('duration', 'Anzeigedauer', ['class' => 'input-group-addon']) !!}
                                    {!! Form::number('duration[]', $pic->duration, ['class' => 'form-control','min'=> 1,'max'=> 30,'id'=>'duration']) !!}
                                    <span class="glyphicon glyphicon-time input-group-addon" aria-hidden="true"></span>
                                </div >
                            </div >
                        @empty
                            <div>Keine Bilder in der Slideshow</div>
                        @endforelse
                        </div>
                        {!! Form::submit('Speichern') !!}
                        {!! Form::close() !!}
                    </div >
                </div >
            </div >
        {{--</div >
    </div >--}}
@endsection
