@extends('layouts.app')

@section('content')
    <div class="container" >
        <div class="row" >
            <div class="" >
                <div class="panel panel-default" >
                    <div class="panel-heading flex flexbetween flexrow" >
                        <div class="cell" >Verarbeitungsschritte einer Slideshow: <br />
                            <i class="fa fa-pencil-square fa-lg" aria-hidden="true"
                               title="Bildauswahl und Reihenfolge" ></i > <i class="fa fa-chevron-right fa-lg"
                                                                             aria-hidden="true" ></i > <i
                                    class="fa fa-clock-o fa-lg" aria-hidden="true" title="Zeiten und Kommentare" ></i >
                            <i class="fa fa-chevron-right fa-lg" aria-hidden="true" ></i ><i
                                    class="fa fa-spinner fa-pulse fa-1x fa-fw" aria-hidden="true"
                                    title="Erstellung" ></i >
                        </div >
                        <div class="cell" >
                            {{ $occasion->name }} - Slideshows: (Name - <span class="badge" >Anzahl an Bildern in der Slideshow</span >)
                        </div >
                        <div class="cell" >
                            Zurücksetzen / Löschen
                        </div >
                    </div >
                    <div class="panel-body" >
                        <ul class="list-group" >
                            @forelse($slideshows as $show)
                                <li class="list-group-item flex flexbetween flexrow" >
                                    <div class="cell" > Status:
                                        @if(array_has(['erstellen'=>1],$show->status))
                                            {!! html_entity_decode(link_to_action('SlideshowsController@edit',
                                                        ' <i class="fa fa-pencil-square fa-lg" aria-hidden="true">'.($show->status == "erstellen" ? '<i class="active-slide" aria-hidden="true" ></i>' : '').'</i>',
                                                        ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                        [ 'title'=>"Slideshow bearbeiten"])) !!}
                                        @else

                                            <i class="fa fa-pencil-square fa-lg" aria-hidden="true" ></i >
                                        @endif
                                        @if(array_has(['bearbeiten'=>1,'erstellen'=>1],$show->status))
                                            {!! html_entity_decode(
                                                    link_to_action('SlideshowsController@time',
                                                        '<i class="fa fa-clock-o fa-lg" aria-hidden="true" >'.($show->status == "bearbeiten" ? '<i class="active-slide" aria-hidden="true" ></i>' : '').'</i>',
                                                        ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                        [ 'title'=>"Anzeigedauer / -text bearbeiten"])) !!}
                                        @else
                                            <i class="fa fa-clock-o fa-lg" aria-hidden="true" ></i >
                                        @endif
                                        @if(array_has(['bearbeiten'=>1,'erstellt'=>1],$show->status) ){!! html_entity_decode(
                                            link_to_action('SlideshowsController@generate',
                                                ''.($show->status == "erstellt" ? '<i class="active-slide" aria-hidden="true" >' : '').'<i class="fa fa-spinner fa-pulse fa-1x fa-fw " aria-hidden="true" ></i>'.($show->status == "erstellt" ? '</i>' : '').'',
                                                ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                [ 'title'=>"Slideshow erzeugen"])) !!}
                                        @else
                                            <i class="fa fa-spinner fa-pulse fa-1x fa-fw " aria-hidden="true" ></i >
                                        @endif
                                    </div >
                                    <div class="cell" >
                                        @php($linkaction = ['erstellen'=>'SlideshowsController@edit','bearbeiten'=>'SlideshowsController@time','erstellt'=>'SlideshowsController@generate'])
                                        {!! html_entity_decode(link_to_action($linkaction[$show->status],
                                                    $show->name.' <span
                                                    class="badge" >'.($show->pictures()->count()).'</span >'.
                                                    ($show->generated ? ' <i class="fa fa-video-camera fa-lg" aria-hidden="true"> </i>' : ''),
                                                    ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                    [ 'title'=>"Slideshow bearbeiten"])) !!}
                                    </div >
                                    <div class="cell" >
                                        {!! html_entity_decode(link_to_action('SlideshowsController@reset',
                                                    '<i class="fa fa-undo fa-lg" aria-hidden="true" ></i >',
                                                    ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                    [ 'title'=>"Slideshow zurücksetzen"])) !!}
                                        {!! html_entity_decode(link_to_action('SlideshowsController@delete',
                                                    '<i class="fa fa-trash fa-lg " aria-hidden="true" ></i >',
                                                    ['slideshow'=>$show->id,'occasion' => $occasion->slug],
                                                    [ 'title'=>"Slideshow löschen",'class' => 'alert-danger','style'=>'margin-left: 2em;'])) !!}
                                    </div >
                                </li >
                            @empty
                                <li class="list-group-item" >Noch keine Slideshows erstellt</li >
                            @endforelse
                        </ul >
                    </div >
                    <div >{!! html_entity_decode(link_to_action('SlideshowsController@create','<i class="fa fa-plus-square fa-lg" aria-hidden="true"> Erstelle eine neue Slideshow</i>',['occasion' => $occasion->slug],['class' => 'btn btn-primary pull-right'])) !!}</div >
                </div >
            </div >
        </div >
    </div >
@endsection
